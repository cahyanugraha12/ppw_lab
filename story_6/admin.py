from django.contrib import admin

from story_6.models import Status, Subscriber

class StatusAdminModel(admin.ModelAdmin):
    list_display = ('content', 'created_at',)

    class Meta:
        model = Status


class SubscriberAdminModel(admin.ModelAdmin):
    list_display = ('email', 'name',)

    class Meta:
        model = Subscriber

admin.site.register(Status, StatusAdminModel)
admin.site.register(Subscriber, SubscriberAdminModel)
