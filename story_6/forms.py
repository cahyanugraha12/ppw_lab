from django import forms
import story_6.models as models

import random

class StatusForm(forms.ModelForm):
    placeholder_messages = ["What's on your mind?", "What's new?", "Anything interesting?", "What's your status?"]
    selected_placeholder = placeholder_messages[random.randint(0, len(placeholder_messages) - 1)]
    
    content = forms.CharField(label='', help_text='',
        widget=forms.Textarea(attrs = {
                'type': 'text', 
                'class': 'form-control', 
                'placeholder': selected_placeholder
            }
        )
    )
    class Meta:
        model = models.Status
        fields = ['content']

class SubscriberForm(forms.ModelForm):
    class Meta:
        model = models.Subscriber
        fields = ['name', 'email', 'password']