from django.db import models


class Status(models.Model):
    content = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = 'Status'
        verbose_name_plural = 'Statuses'

class Subscriber(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=100)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Subscriber'
        verbose_name_plural = 'Subscribers'