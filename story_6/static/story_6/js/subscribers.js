const errorMessage = 'Fetching subscriber list failed. Please try again.';
const subsListContainer = $('#subscriber-list-container');
const baseURL = $(subsListContainer).data('url');

$(document).ready(function() {
    getSubscriber();
    deleteSubsModalButtonListener();
});

function subsListLoading() {
    $('#loader').show();
    $(subsListContainer).hide();
}

function subsListLoaded() {
    $('#loader').hide();
    $(subsListContainer).show();
}

function subsListLoadError() {
    $('#loader').hide();
    $(`<h1>${errorMessage}</h1>`).insertAfter(subsListContainer);
}

function generateSubsList(data, callback) {
    $(subsListContainer).empty();
    let subsHTML = ``;
    data.forEach((item) => {
        const id = item.id;
        const name = item.name;
        const email = item.email;
        subsHTML = subsHTML.concat(`
            <div class="card bg-primair" id="subs-id-${ id }">
                <div class="card-body">
                    <h5 class="card-title">${ name }</h5>
                    <h6 class="card-text">${ email }</h6>
                </div>
                <div class="card-footer">
                    <button type="button" class="btn bg-accent button-delete-subscriber" data-subs-id=${ id }>Delete</button>
                </div>
            </div>
        `);
    })
    $(subsListContainer).append(subsHTML);
    subsListLoaded();
    callback();
}

function getSubscriber() {
    subsListLoading()
    $.ajax(baseURL, {
        success: function(data) {
            generateSubsList(data, deleteSubsButtonListener)
        },
        failure: subsListLoadError
    });
}

function deleteSubsButtonListener() {
    $(".button-delete-subscriber").click(function() {
        $("#modal-delete-subscriber").attr("data-cur-subs-id", $(this).data("subs-id"));
        $("#modal-delete-subscriber").modal({
            keyboard: true
        });
    });
}

function deleteSubsModalButtonListener() {
    $("#modal-button-delete-subscriber").click(function() {
        const id = $("#modal-delete-subscriber").data("cur-subs-id");
        const password = $("#modal-password-delete-subscriber").val();
        $.ajax(baseURL + "?id=" + id, {
            success: function(data) {
                if (password === data.password) {
                    doDeleteSubscriber(id);
                } else {
                    $("#modal-password-delete-subscriber").addClass("invalid");
                    $("#modal-password-delete-subscriber-invalid-feedback").show();
                }
            }
        });
    })
}

function doDeleteSubscriber(id) {
    const csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajax(baseURL + "?id=" + id, {
        type: "DELETE",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        xhrFields: {
            withCredentials: true
        },
        success: function(data) {
            const subscriberCard = "#subs-id-" + id;
            $(subscriberCard).remove();
            $("#modal-delete-subscriber").modal('toggle');
        }
    });
}