document.onreadystatechange = function() {
    setThemeOnFront();
    showLoader();
    if (document.readyState === "complete")  {
        setTimeout(function() {
            hideLoader();
        }, 500);
    }
}

function showLoader() {
    $("#loader").show();
    $("nav").hide();
    $(".content").hide();
    $("footer").hide();
}

function hideLoader() {
    $("#loader").hide();
    $("nav").show();
    $(".content").show();
    $("footer").show();
}

function localStorageSetThemeOnBack(theme) {
    localStorage.setItem('theme', theme);
    setThemeOnFront();
}

function setThemeOnFront() {
    if (localStorage.getItem('theme') === "ui-lover") {
        $("html").removeClass(function (index, className) {
            return (className.match (/(^|\s)theme-\S+/g) || []).join(' ');
        }).addClass("theme-ui-lover");

        $("nav").removeClass("navbar-dark").addClass("navbar-light");
    } else {
        $("html").removeClass(function (index, className) {
            return (className.match (/(^|\s)theme-\S+/g) || []).join(' ');
        }).addClass("theme-fasilkom-lover");

        $("nav").removeClass("navbar-light").addClass("navbar-dark");
    }
}