/* Saya cinta callback hell */
/* Anti promise promise club */
const bookTableId = '#book-table';
const bookTableBodyId = '#book-table-body'
const baseBookUrl = $(bookTableId).data("url");
const errorMessage = 'Fetching book data failed. Please try again.';

$(document).ready(function() {
    getBook("quilting", doInitialInitialization);
});

function tableLoading() {
    $('#loader').show();
    $(bookTableId).hide();
}

function tableLoaded() {
    $('#loader').hide();
    $(bookTableId).show();
}

function tableLoadError() {
    $('#loader').hide();
    $(`<h1>${errorMessage}</h1>`).insertAfter(bookTableId);
}

function generateBookTable(data, callback) {
    $(bookTableBodyId).empty();
    const items = data.items;
    let tableHTML = ``;
    items.forEach((item) => {
        let title = item.volumeInfo.title;
        if (item.volumeInfo.subtitle !== undefined) {
            title = title.concat(' - ', item.volumeInfo.subtitle);
        }
        let description = (item.volumeInfo.description !== undefined) ? item.volumeInfo.description : "None";
        let authors = "None";
        if (item.volumeInfo.authors !== undefined) {
            item.volumeInfo.authors.forEach((author) => {
                if (authors === "None") {
                    authors = author;
                } else {
                    authors += ", " + author;
                }
            });
        }
        let publisher = (item.volumeInfo.publisher !== undefined) ? item.volumeInfo.publisher : "None";
        let publishedDate = (item.volumeInfo.publishedDate !== undefined) ? item.volumeInfo.publishedDate : "None";
        tableHTML = tableHTML.concat(`
            <tr class="favorite-book-row" data-book-row-id="${ item.id }">
                <td>
                    ${title}
                </td>
                <td>
                    ${description}
                </td>
                <td>
                    ${authors}
                </td>
                <td>
                    ${publisher}
                </td>
                <td>
                    ${publishedDate}
                </td>
                <td class="text-center">
                    <i class="far fa-star fa-2x clickable favorite-button" data-favorite-button-id="${ item.id }"></i>
                </td>
            </tr>
        `);
    })
    $(bookTableBodyId).append(tableHTML);
    tableLoaded();
    callback();
}

function getBook(query, callbackToGenerateBook) {
    tableLoading();
    let bookURL = baseBookUrl;
    if (query !== undefined) {
        bookURL += "?q=" + query;
    } else {
        bookURL += "?q=" + "quilting";
    }
    $.ajax(bookURL, {
        success: function(data) {
            generateBookTable(data, callbackToGenerateBook);
        },
        error: tableLoadError
    }) 
}

function doInitialInitialization() {
    reset();
    favoriteButtonListener();
    showFavoriteTogglerListener();
    searchButtonListener();
}

function doSubsequentInitialization() {
    reset();
    favoriteButtonListener();
}

function toggleBookFavoriteStar(element) {
    $(element).toggleClass("far");
    $(element).toggleClass("fas");
}

function updateFavoriteAmount(amount) {
    $(".favorite-amount").attr("data-amount", amount);
}

function reset() {
    initFromLocalStorage();

    if ($(".favorite-only-toggler").hasClass("active")) {
        $(".favorite-only-toggler").toggleClass("active");
    }
}

function initFromLocalStorage() {
    let favoriteList = JSON.parse(localStorage.getItem("favorite-list"));
    let amount = 0;
    if (favoriteList !== undefined && favoriteList !== null) {
        $.each($(".favorite-button"), function() {
            if ($.inArray($(this).data("favorite-button-id"), favoriteList) !== -1) {
                toggleBookFavoriteStar(this);
                amount++;
            }
        });
    } else {
        favoriteList = [];
        localStorage.setItem("favorite-list", JSON.stringify(favoriteList));
    }

    updateFavoriteAmount(amount);
}

function favoriteButtonListener() {
    $(".favorite-button").click(function() {
        toggleBookFavoriteStar(this);

        let amount = parseInt($(".favorite-amount").attr("data-amount"));
        if (amount === undefined || amount === null) {
            amount = 0;
        }
        let favoriteList = JSON.parse(localStorage.getItem("favorite-list"));
        const indexOfThisId = $.inArray($(this).data("favorite-button-id"), favoriteList);

        if (indexOfThisId !== -1) {
            amount = amount - 1;
            favoriteList.splice(indexOfThisId, 1);
        } else {
            amount = amount + 1;
            favoriteList.push($(this).data("favorite-button-id"));
        }
        updateFavoriteAmount(amount);
        localStorage.setItem("favorite-list", JSON.stringify(favoriteList));

        const active = $(".favorite-only-toggler").hasClass("active");
        toggleFavoriteBookRow(active);
    });
}

function toggleFavoriteBookRow(active) {
    const favoriteList = JSON.parse(localStorage.getItem("favorite-list"));
    $.each($(".favorite-book-row"), function() {
        const indexOfThisId = $.inArray($(this).data("book-row-id"), favoriteList);
        if (indexOfThisId === -1 && active) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
}

function showFavoriteTogglerListener() {
    $(".favorite-only-toggler").click(function() {
        $(this).toggleClass("active");
        toggleFavoriteBookRow($(this).hasClass("active"));
    });
}

function searchButtonListener() {
    $("#search-button").click(function() {
        const query = $("#search-value").val();
        getBook(query, doSubsequentInitialization);
    });
}