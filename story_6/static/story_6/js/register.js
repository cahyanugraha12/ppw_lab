const VALID_EMAIL_AVAILABLE = "Email is available.";
const INVALID_NOT_FILLED = "This field cannot be empty.";
const INVALID_EMAIL_ALREADY_REGISTERED = "Email already registered.";
const INVALID_EMAIL_WRONG_FORMAT = "Email format incorrect.";

$(document).ready(function() {
    nameFieldListener();
    emailFieldListener();
    passwordFieldListener();
    registerListener();
    submitButtonChecker();
});

function nameFieldChecker() {
    const nameField = $("#registration-name");
    const nameFieldNonJQuery = document.getElementById("registration-name");
    if (!$(nameField).val()) {
        $("#invalid-feedback-name").show();
        $("#invalid-feedback-name").children().attr("data-message", INVALID_NOT_FILLED);
        $(nameField).addClass("invalid");
        $(nameField).removeClass("valid");
    } else if (nameFieldNonJQuery.checkValidity()) {
        $("#invalid-feedback-name").hide();
        $(nameField).addClass("valid");
        $(nameField).removeClass("invalid");
    }
}

function nameFieldListener() {
    const nameField = $("#registration-name");
    $(nameField).change(function() {
        nameFieldChecker();
        submitButtonChecker();
    });
}

function emailFieldNotExistChecker() {
    const emailField = $("#registration-email");
    const emailFieldNonJQuery = document.getElementById("registration-email");
    if (!$(emailField).val()) {
        $("#valid-feedback-email").hide();
        $("#invalid-feedback-email").show();
        $("#invalid-feedback-email").children().attr("data-message", INVALID_NOT_FILLED);
        $(emailField).addClass("invalid");
        $(emailField).removeClass("valid");
    }
    else if (!emailFieldNonJQuery.checkValidity()) {
        $("#valid-feedback-email").hide();
        $("#invalid-feedback-email").show();
        $("#invalid-feedback-email").children().attr("data-message", INVALID_EMAIL_WRONG_FORMAT);
        $("#invalid-feedback-email-not-filled").hide();
        $(emailField).addClass("invalid");
        $(emailField).removeClass("valid");
    } else {
        $("#valid-feedback-email").show();
        $("#valid-feedback-email").children().attr("data-message", VALID_EMAIL_AVAILABLE);
        $("#invalid-feedback-email").hide();
        $(emailField).addClass("valid");
        $(emailField).removeClass("invalid");
    }
}

function emailFieldListener() {
    const emailField = $("#registration-email");
    $(emailField).change(function() {
        const baseUrl = $(emailField).data("url");
        const value = $(emailField).val();
        const url = baseUrl + "?email=" + value;
        $.ajax(url, {
            success: function(data) {
                if (data.exist) {
                    $("#valid-feedback-email").hide();
                    $("#invalid-feedback-email").show();
                    $("#invalid-feedback-email").children().attr("data-message", INVALID_EMAIL_ALREADY_REGISTERED);
                    $(emailField).addClass("invalid");
                    $(emailField).removeClass("valid");
                } else {
                    emailFieldNotExistChecker();
                }
                submitButtonChecker();
            }
        });
    });
}

function passwordFieldChecker() {
    const passwordField = $("#registration-password");
    const passwordFieldNonJQuery = document.getElementById("registration-password");
    if (!$(passwordField).val()) {
        $("#invalid-feedback-password").show();
        $("#invalid-feedback-password").children().attr("data-message", INVALID_NOT_FILLED);
        $(passwordField).addClass("invalid");
        $(passwordField).removeClass("valid");
    }
    else if (passwordFieldNonJQuery.checkValidity()) {
        $("#invalid-feedback-password").hide();
        $(passwordField).addClass("valid");
        $(passwordField).removeClass("invalid");
    }
}

function passwordFieldListener() {
    const passwordField = $("#registration-password");
    $(passwordField).change(function() {
        passwordFieldChecker();
        submitButtonChecker();
    });
}

function submitButtonChecker() {
    const nameFieldValid = $("#registration-name").hasClass("valid");
    const emailFieldValid = $("#registration-email").hasClass("valid");
    const passwordFieldValid = $("#registration-password").hasClass("valid");
    if (nameFieldValid && emailFieldValid && passwordFieldValid) {
        $("#button-submit").prop("disabled", false);
        $("#button-submit").removeClass("disabled");
    } else {
        $("#button-submit").prop("disabled", true);
        $("#button-submit").addClass("disabled");
    }
}

function resetNameField() {
    const nameField = $("#registration-name");
    $(nameField).val("");
    $("#invalid-feedback-name-not-filled").hide();
    $(nameField).removeClass("valid");
    $(nameField).removeClass("invalid");
}

function resetEmailField() {
    const emailField = $("#registration-email");
    $(emailField).val("");
    $("#valid-feedback-email").hide();
    $("#invalid-feedback-email-wrong-format").hide();
    $("#invalid-feedback-email-already-exist").hide();
    $("#invalid-feedback-email-not-filled").hide();
    $(emailField).removeClass("valid");
    $(emailField).removeClass("invalid");
}

function resetPasswordField() {
    const passwordField = $("#registration-password");
    $(passwordField).val("");
    $("#invalid-feedback-password-not-filled").hide();
    $(passwordField).removeClass("valid");
    $(passwordField).removeClass("invalid");
}

function resetForm() {
    resetNameField();
    resetEmailField();
    resetPasswordField();
    submitButtonChecker();
}

function showRegistrationSuccessModal() {
    $("#registration-success").modal({
        keyboard: true
    });
}

function showRegistrationFailedModal() {
    $("#registration-failed").modal({
        keyboard: true
    });
}

function registerListener() {
    $("#button-submit").click(function() {
        const url = $("#registration-form").data("url")
        const csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        const data = {
            "name": $("#registration-name").val(),
            "email": $("#registration-email").val(),
            "password": $("#registration-password").val()
        }
        $.ajax(url, {
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            },
            xhrFields: {
                withCredentials: true
            },
            data: data,
            success: function(data) {
                if (data.created) {
                    showRegistrationSuccessModal();
                    resetForm();
                } else {
                    showRegistrationFailedModal();
                }
            },
            error: showRegistrationFailedModal
        });
    });
}