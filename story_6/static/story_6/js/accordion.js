$(document).ready(function() {
    accordionListener();
});

function accordionListener() {
    $(".accordion-toggler").click(function() {
        $(this).siblings(".accordion-panel").each(function() {
            $(this).slideToggle();
        });
        $(this).toggleClass("active");
    });
}