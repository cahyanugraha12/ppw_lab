from django.urls import re_path
from story_6.views import homepage, profile, books, books_api, \
                          register, check_email, subscribers, subscribers_api, \
                          logout_view, get_session_data_ajax, set_session_data_ajax

urlpatterns = [
    re_path(r'^$', homepage, name='homepage'),
    re_path(r'^profile$', profile, name='profile'),
    re_path(r'^books$', books, name='books'),
    re_path(r'^books-api/', books_api, name='books-api'),
    re_path(r'^register$', register, name='register'),
    re_path(r'^check-email/', check_email, name='check-email'),
    re_path(r'^subscribers$', subscribers, name='subscribers'),
    re_path(r'^subscribers-api/', subscribers_api, name='subscribers-api'),
    re_path(r'^logout$', logout_view, name='logout'),
    re_path(r'^get-session-api', get_session_data_ajax, name="get-session-data-ajax"),
    re_path(r'^set-session-api', set_session_data_ajax, name="set-session-data-ajax"),
]