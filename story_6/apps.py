from django.apps import AppConfig


class Story6Config(AppConfig):
    name = 'story_6'
    verbose_name = "Story"
