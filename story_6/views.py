from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.contrib.auth import logout
from story_6.models import Status, Subscriber
from story_6.forms import StatusForm, SubscriberForm

import requests
import json

def homepage(request):
    status_list = Status.objects.all().order_by('-created_at')
    status_form = StatusForm(request.POST or None)
    
    if request.method == 'POST':
        if status_form.is_valid():
            status_form.save()
            return redirect('homepage')
    response = {
        'status_list': status_list,
        'status_form': status_form,
        'title': 'Pande Ketut Cahya Nugraha'
    }

    return render(request, 'story_6/homepage.html', response)

# Story 8
def profile(request):
    response = {
        'title': 'Pande Ketut Cahya Nugraha'
    }
    return render(request, 'story_6/profile.html', response)

# Story 9
def books(request):
    response = {
        'title': 'Pande Ketut Cahya Nugraha'
    }
    return render(request, 'story_6/books.html', response)

def books_api(request):
    query = request.GET.get('q', 'quilting')
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + query
    data = requests.get(url)
    data = data.json()

    return JsonResponse(data)

# Story 10
def register(request):
    if request.method == 'POST':
        data = request.POST.copy()
        subs_form = SubscriberForm(data=data)
        response = {
            'created': True
        }
        if subs_form.is_valid() and not Subscriber.objects.filter(email=data["email"]).exists():
            # Password kamu kusimpan plaintext, Pulgoso. Huahahahaha.
            subs_form.save()
        else:
            response['created'] = False

        return JsonResponse(response)
    else:
        response = {
            'title': 'Pande Ketut Cahya Nugraha'
        }
        return render(request, 'story_6/register.html', response)

def check_email(request):
    query = request.GET.get('email', '')
    response = {
        'exist': False
    }
    if Subscriber.objects.filter(email=query).exists():
        response['exist'] = True

    return JsonResponse(response)

def subscribers(request):
    response = {
        'title': 'Pande Ketut Cahya Nugraha'
    }
    return render(request, 'story_6/subscribers.html', response)

def subscribers_api(request):
    query = request.GET.get('id', None)
    if request.method == "DELETE":
        response = {
            'deleted': False
        }

        if query is not None:
            query = int(query)
            subscriber = Subscriber.objects.get(id=query).delete()
            response['deleted'] = True

        return JsonResponse(response)
    else:
        if query is None:
            subscribers_list = Subscriber.objects.all()
            response = []
            for item in subscribers_list.values():
                response.append(item)
            return JsonResponse(response, safe=False)
        else:
            query = int(query)
            subscriber = Subscriber.objects.get(id=query)
            response = {
                'id': subscriber.id,
                'name': subscriber.name,
                'email': subscriber.email,
                'password': subscriber.password
            }
            return JsonResponse(response)
    
# Story 11
def logout_view(request):
    logout(request)
    return redirect('homepage')

def get_session_data_ajax(request):
    if request.method == 'GET':
        query = request.GET.getlist('q')
        response = {
            'success': True
        }

        allowed_key = ['favorite-list']

        for q in query:
            if request.session.get(q, False) and q in allowed_key:
                response[q] = request.session[q]
            else:
                response[q] = None
        
        return JsonResponse(response)

def set_session_data_ajax(request):
    if request.method == 'POST':
        data = request.POST.copy()
        for key, value in data.items():
            print(key, value)
            request.session[key] = value

        response = {
            'success': True
        }

        return JsonResponse(response)

def set_extra_data_on_session(backend, strategy, details, response,
                              user=None, *args, **kwargs):
    if backend.name == 'google-oauth2':
        strategy.session_set('id_token', response['id_token'])
        strategy.session_set('email', response['emails'][0].get('value'))
        strategy.session_set('display_name', response['displayName'])
        strategy.session_set('image_url', response['image'].get('url'))
        