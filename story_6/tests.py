from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.conf import settings

import pytz
from unittest import mock
from datetime import datetime
import time
import requests
import json

from story_6.models import Status, Subscriber
from story_6.forms import StatusForm, SubscriberForm
from story_6.views import homepage, profile, books, books_api, register, check_email, subscribers, subscribers_api, logout_view, get_session_data_ajax, set_session_data_ajax, set_extra_data_on_session

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Story6ModelsTestCase(TestCase):
    def test_status_created_correctly(self):
        status_content = ("Kata seseorang yang bijak kerjakanlah hal-hal "
                          "yang membuatmu senang. PPW membuat saya stres.")
        time_now = datetime(2018, 12, 31, 0, 0, 0, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=time_now)):
            status = Status(content=status_content)
            status.save()

        self.assertEqual(status.content, status_content)
        self.assertEqual(status.created_at, time_now)
    
    def test_status_inserted_into_database(self):
        status = Status(content="Test")
        status.save()

        database_count = Status.objects.all().count()
        self.assertEqual(database_count, 1)
    
    def test_status_str_is_content(self):
        status = Status(content="Test")

        self.assertEqual(str(status), status.content)


class Story6FormsTestCase(TestCase):
    def test_status_form_valid(self):
        data = { 
            'content': ("Kata seseorang yang bijak kerjakanlah hal-hal "
                        "yang membuatmu senang. PPW membuat saya stres.")
        }
        form = StatusForm(data=data)
        self.assertTrue(form.is_valid())

    def test_status_form_invalid_content_blank(self):
        data = {
            'content': ""
        }
        form = StatusForm(data=data)
        self.assertFalse(form.is_valid())

    def test_status_form_invalid_content_exceed_max_length(self):
        data = {
            'content': ("CUS9l426r7HT40eAnbQmFo6ZfRehTVN85nRCkkFurk6E3hG7V"
                        "H5anW3q1lDRBG4TAhqLqU4V0UiA5om5n4wcVO6CayGtvjXyQg"
                        "QrCIn29bf5QnAstNIWN44XmOShcKJjDHW2vHJuaBggkUYZWoVp"
                        "Nhfq5pbyF3EwNKvtd5hM982K6aU3XU7GRAv0w6XRtcToP6qJm7"
                        "1fnnXfk7oQaNSkD11vEgLtfUuku3Mibugy1PolbwPZRsgR8z81"
                        "rPRN2COz1KKruxj7rJc6dHBdZCCvqxk26gvUjINl7kqUmlyMk4a2r")
        }
        form = StatusForm(data=data)
        self.assertFalse(form.is_valid())


class Story6ViewsTestCase(TestCase):
    def test_homepage_url_exists(self):
        response = Client().get(reverse('homepage'))
        self.assertEqual(response.status_code, 200)

    def test_homepage_url_use_homepage_view(self):
        homepage_url = resolve(reverse('homepage'))
        self.assertEqual(homepage_url.func, homepage)

    def test_homepage_view_no_status_render_correctly(self):
        empty_form = StatusForm()

        response = Client().get(reverse('homepage'))
        self.assertTemplateUsed(response, 'story_6/homepage.html')
        self.assertContains(response, 'Hello, Apa kabar?')
        self.assertContains(response, empty_form)
    
    def test_homepage_view_post_status_success(self):
        data = {
            'content': "Ini status yang harusnya berhasil ditampilkan"
        }
        response = Client().post(reverse('homepage'), data=data)
        self.assertEqual(response.status_code, 302)

        response = Client().get(reverse('homepage'))
        self.assertContains(response, data['content'])
    
    def test_homepage_view_post_status_failed(self):
        data = {
            'content': ("Ini status yang harusnya tidak berhasil ditampilkan karena melebihi 300 karakter "
                        "CUS9l426r7HT40eAnbQmFo6ZfRehTVN85nRCkkFurk6E3hG7V"
                        "H5anW3q1lDRBG4TAhqLqU4V0UiA5om5n4wcVO6CayGtvjXyQg"
                        "QrCIn29bf5QnAstNIWN44XmOShcKJjDHW2vHJuaBggkUYZWoVp"
                        "Nhfq5pbyF3EwNKvtd5hM982K6aU3XU7GRAv0w6XRtcToP6qJm7"
                        "1fnnXfk7oQaNSkD11vEgLtfUuku3Mibugy1PolbwPZRsgR8z81"
                        "rPRN2COz1KKruxj7rJc6dHBdZCCvqxk26gvUjINl7kqUmlyMk4a2r")
        }
        response = Client().post(reverse('homepage'), data=data)
        self.assertEqual(response.status_code, 200)

        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 0)
    
    def test_profile_url_exists(self):
        response = Client().get(reverse('profile'))
        self.assertEqual(response.status_code, 200)

    def test_profile_url_use_profile_view(self):
        profile_url = resolve(reverse('profile'))
        self.assertEqual(profile_url.func, profile)
    
    def test_profile_use_correct_template(self):
        response = Client().get(reverse('profile'))
        self.assertTemplateUsed(response, 'story_6/profile.html')

    def test_profile_view_correct_content(self):
        response = Client().get(reverse('profile'))
        self.assertContains(response, 'Profile')
        self.assertContains(response, 'Pande Ketut Cahya Nugraha')
        self.assertContains(response, 'Contacts')
        self.assertContains(response, 'Email')
        self.assertContains(response, 'Phone Number')
        self.assertContains(response, 'Bio')

class Story7FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_can_post_new_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        content = selenium.find_element_by_name('content')
        save_button = selenium.find_element_by_name('status-form-submit-button')
        
        content.send_keys('Waw selenium')
        save_button.send_keys(Keys.RETURN)

        self.assertIn('Waw selenium', selenium.page_source)
    
    def test_cant_post_new_status_that_exceed_300_char(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        content = selenium.find_element_by_name('content')
        save_button = selenium.find_element_by_name('status-form-submit-button')
        
        content.send_keys('wadidaw' * 50)
        save_button.send_keys(Keys.RETURN)

        self.assertIn('Ensure this value has at most 300 characters', selenium.page_source)
    
    def test_title_is_correct_and_inside_head(self):
        correct_title = 'Pande Ketut Cahya Nugraha'
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        title = selenium.find_element_by_xpath('//head[@name="head"]/title').get_attribute('innerHTML')
        self.assertIn(correct_title, title)
    
    def test_status_card_is_inside_status_container_and_displayed_correctly(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        content = selenium.find_element_by_name('content')
        save_button = selenium.find_element_by_name('status-form-submit-button')
        
        content_status = 'Waw selenium'
        content.send_keys(content_status)
        save_button.send_keys(Keys.RETURN)

        # Select first card that is within a div with attribute name which value is status-container
        status_card_xpath = '//div[@name="status-container"]/div[contains(@class, "card")][1]'
        status_card = selenium.find_element_by_xpath(status_card_xpath).get_attribute('innerHTML')

        self.assertIn(content_status, status_card)

    def test_check_body_background_color_and_text_color(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        body = selenium.find_element_by_xpath('//body')
        body_background_color = body.value_of_css_property('background-color')
        body_text_color = body.value_of_css_property('color')

        self.assertIn('(55, 90, 127, 1)', body_background_color)
        self.assertIn('(255, 255, 255, 1)', body_text_color)
    
    def test_check_body_css_class_is_correct(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        body = selenium.find_element_by_xpath('//body')
        body_css_class = body.get_attribute('class')
        
        self.assertIn('bg-dominant', body_css_class)

    def test_status_container_css_class_is_correct(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        status_container = selenium.find_element_by_name("status-container")
        status_container_css_class = status_container.get_attribute('class')

        self.assertIn('col-sm-8', status_container_css_class)
    
class Story8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_can_change_theme(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(1)

        navbar_button = selenium.find_element_by_class_name('navbar-toggler')
        navbar_button.click()
        time.sleep(1)

        theme_dropdown = selenium.find_element_by_id('navbarDropdownTheme')
        theme_dropdown.click()
        time.sleep(1)

        ui_lover_theme_button = selenium.find_element_by_name("ui-lover-selector")
        ui_lover_theme_button.click()
        time.sleep(1)

        html = selenium.find_element_by_tag_name('html')
        html_class = html.get_attribute('class')
        self.assertIn('theme-ui-lover', html_class)

    def test_profile_accordion_work_properly(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile')
        time.sleep(1)

        accordion_panels = selenium.find_elements_by_class_name('accordion-panel')
        for panel in accordion_panels:
            panel_display = panel.value_of_css_property('display')
            self.assertIn('none', panel_display)

        accordion_togglers = selenium.find_elements_by_class_name('accordion-toggler')
        for toggler in accordion_togglers:
            toggler.click()
            time.sleep(1)
        
        for panel in accordion_panels:
            panel_display = panel.value_of_css_property('display')
            self.assertIn('block', panel_display)
    
    def test_check_spinner_loading_correctly(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile')

        loader = selenium.find_element_by_id('loader')
        loader_display = loader.value_of_css_property('display')
        self.assertIn('block', loader_display)

        time.sleep(5)   # Let the page load completely so loader disappear

        loader_display = loader.value_of_css_property('display')
        self.assertIn('none', loader_display)

class Story9ViewsTestCase(TestCase):
    def test_books_url_exists(self):
        response = Client().get(reverse('books'))
        self.assertEqual(response.status_code, 200)

    def test_books_url_use_register_view(self):
        books_url = resolve(reverse('books'))
        self.assertEqual(books_url.func, books)
    
    def test_books_use_correct_template(self):
        response = Client().get(reverse('books'))
        self.assertTemplateUsed(response, 'story_6/books.html')

    def test_books_api_url_exists(self):
        response = Client().get(reverse('books-api'))
        self.assertEqual(response.status_code, 200)

    def test_books_api_url_use_homepage_view(self):
        books_api_url = resolve(reverse('books-api'))
        self.assertEqual(books_api_url.func, books_api)
    
    def test_books_api_return_correct_data(self):
        url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
        data = requests.get(url)
        data = data.json()

        response = Client().get(reverse('books-api'))
        self.assertEqual(response.json(), data)

class Story9FunctionalTest(TestCase):
    def setUp(self):
        settings.CSRF_COOKIE_SECURE = False
        settings.SESSION_COOKIE_SECURE = False
        
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story9FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story9FunctionalTest, self).tearDown()

    def test_favorite_button_function_correctly_and_amount_favorite_displayed(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/books')

        time.sleep(5)

        favorite_buttons = selenium.find_elements_by_class_name('favorite-button')
        active_favorite_button_class = "fas"
        count_favorite_button = 0
        for favorite_button in favorite_buttons:
            favorite_button.click()
            time.sleep(5)
            count_favorite_button += 1
            button_class = favorite_button.get_attribute('class')
            self.assertIn(active_favorite_button_class, button_class)
        
        favorite_amount = selenium.find_element_by_class_name('favorite-amount')
        favorite_amount_data = favorite_amount.get_attribute('data-amount')
        self.assertIn(str(count_favorite_button), favorite_amount_data)
    
    def test_button_show_favorites_only_function_correctly(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/books')

        time.sleep(5)

        favorite_buttons = selenium.find_elements_by_class_name('favorite-button')
        favorite_buttons[0].click()
        selected_favorite_row_id = favorite_buttons[0].get_attribute('data-favorite-button-id')

        show_favorite_only_button = selenium.find_element_by_class_name('favorite-only-toggler')
        show_favorite_only_button_class = show_favorite_only_button.get_attribute('class')
        self.assertNotIn('active', show_favorite_only_button_class)

        show_favorite_only_button.click()
        show_favorite_only_button_class = show_favorite_only_button.get_attribute('class')
        self.assertIn('active', show_favorite_only_button_class)

        time.sleep(5)

        books_row = selenium.find_elements_by_class_name('favorite-book-row')
        for row in books_row:
            row_display = row.value_of_css_property('display')
            row_id = row.get_attribute('data-book-row-id')
            if row_id == selected_favorite_row_id:
                self.assertIn('table-row', row_display)
            else:
                self.assertIn('none', row_display)

class LastStoryModelsTestCase(TestCase):
    def test_subscriber_created_correctly(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        subscriber = Subscriber(**data)

        self.assertEqual(subscriber.name, data['name'])
        self.assertEqual(subscriber.email, data['email'])
        self.assertEqual(subscriber.password, data['password'])
    
    def test_subscriber_inserted_into_database(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        subscriber = Subscriber(**data)
        subscriber.save()

        database_count = Subscriber.objects.all().count()
        self.assertEqual(database_count, 1)
    
    def test_subscriber_str_is_email(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        subscriber = Subscriber(**data)

        self.assertEqual(str(subscriber), subscriber.email)

class LastStoryFormsTestCase(TestCase):
    def test_subscriber_form_valid(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        form = SubscriberForm(data=data)
        self.assertTrue(form.is_valid())

    def test_subscriber_form_invalid_blank(self):
        data = {
            'name': '',
            'email': '',
            'password': ''
        }
        form = SubscriberForm(data=data)
        self.assertFalse(form.is_valid())

class LastStoryViewsTestCase(TestCase):
    def test_register_url_exists(self):
        response = Client().get(reverse('register'))
        self.assertEqual(response.status_code, 200)

    def test_register_url_use_register_view(self):
        register_url = resolve(reverse('register'))
        self.assertEqual(register_url.func, register)
    
    def test_register_use_correct_template(self):
        response = Client().get(reverse('register'))
        self.assertTemplateUsed(response, 'story_6/register.html')

    def test_register_can_save_valid_post(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }

        response = Client().post(reverse('register'), data=data)
        database_count = Subscriber.objects.all().count()
        self.assertEqual(database_count, 1)
    
    def test_register_wont_save_invalid_post(self):
        data = {
            'name': '',
            'email': '',
            'password': ''
        }

        response = Client().post(reverse('register'), data=data)
        database_count = Subscriber.objects.all().count()
        self.assertEqual(database_count, 0)

    def test_check_email_api_url_exists(self):
        response = Client().get(reverse('check-email'))
        self.assertEqual(response.status_code, 200)

    def test_check_email_api_url_use_check_email_view(self):
        check_email_url = resolve(reverse('check-email'))
        self.assertEqual(check_email_url.func, check_email)
    
    def test_check_email_api_default_return_false(self):
        response = Client().get(reverse('check-email'))
        response = response.json()
        self.assertFalse(response['exist'])

    def test_check_email_api_return_false_for_non_existing_email(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }

        response = Client().get(reverse('check-email') + "?email=" + data['email'])
        response = response.json()
        self.assertFalse(response['exist'])

    def test_check_email_api_return_true_for_existing_email(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        subscriber = Subscriber(**data)
        subscriber.save()

        response = Client().get(reverse('check-email') + "?email=" + data['email'])
        response = response.json()
        self.assertTrue(response['exist'])
    
    def test_subscribers_url_exists(self):
        response = Client().get(reverse('subscribers'))
        self.assertEqual(response.status_code, 200)

    def test_subscribers_url_use_subscribers_view(self):
        subscribers_url = resolve(reverse('subscribers'))
        self.assertEqual(subscribers_url.func, subscribers)
    
    def test_subscribers_use_correct_template(self):
        response = Client().get(reverse('subscribers'))
        self.assertTemplateUsed(response, 'story_6/subscribers.html')
    
    def test_subscribers_api_url_exists(self):
        response = Client().get(reverse('subscribers-api'))
        self.assertEqual(response.status_code, 200)

    def test_subscribers_api_url_use_subscribers_view(self):
        subscribers_api_url = resolve(reverse('subscribers-api'))
        self.assertEqual(subscribers_api_url.func, subscribers_api)
    
    def test_subcribers_api_return_all_subscriber(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        subscriber = Subscriber(**data)
        subscriber.save()

        data_2 = {
            'name': 'Nugraha',
            'email': 'ahaha@gmail.com',
            'password': 'KepoPol'
        }
        subscriber_2 = Subscriber(**data_2)
        subscriber_2.save()

        response = Client().get(reverse('subscribers-api'))
        self.assertContains(response, subscriber.id)
        self.assertContains(response, subscriber.name)
        self.assertContains(response, subscriber.email)
        self.assertContains(response, subscriber.password)
        self.assertContains(response, subscriber_2.id)
        self.assertContains(response, subscriber_2.name)
        self.assertContains(response, subscriber_2.email)
        self.assertContains(response, subscriber_2.password)
    
    def test_subcribers_api_return_single_subscriber(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        subscriber = Subscriber(**data)
        subscriber.save()

        response = Client().get(reverse('subscribers-api') + "?id=" + str(subscriber.id))
        self.assertContains(response, subscriber.id)
        self.assertContains(response, subscriber.name)
        self.assertContains(response, subscriber.email)
        self.assertContains(response, subscriber.password)
    
    def test_subscriber_api_can_delete_single_subscriber(self):
        data = {
            'name': 'Cahya',
            'email': 'cahyanugraha12@gmail.com',
            'password': 'KepoPol'
        }
        subscriber = Subscriber(**data)
        subscriber.save()

        response = Client().delete(reverse('subscribers-api') + "?id=" + str(subscriber.id))

        database_count = Subscriber.objects.all().count()
        self.assertEqual(database_count, 0)
    
class Story11ViewsTestCase(TestCase):
    def test_logout_url_exists(self):
        response = Client().get(reverse('logout'))
        self.assertEqual(response.status_code, 302)

    def test_logout_url_use_logout_view(self):
        logout_url = resolve(reverse('logout'))
        self.assertEqual(logout_url.func, logout_view)

    def test_get_session_data_ajax_url_exists(self):
        response = Client().get(reverse('get-session-data-ajax'))
        self.assertEqual(response.status_code, 200)

    def test_get_session_data_ajax_url_use_get_session_data_ajax_view(self):
        get_session_data_ajax_url = resolve(reverse('get-session-data-ajax'))
        self.assertEqual(get_session_data_ajax_url.func, get_session_data_ajax)

    def test_get_session_data_ajax_url_can_get_session_with_allowed_key(self):
        session = self.client.session
        session['favorite-list'] = ['Good Book']
        session.save()

        response = self.client.get(reverse('get-session-data-ajax') + "?q=favorite-list")
        
        self.assertEqual(json.loads(response.getvalue().decode())['favorite-list'], session['favorite-list'])

    def test_get_session_data_ajax_url_cant_get_session_with_disallowed_key(self):
        session = self.client.session
        session['user-id'] = ['Kepo']
        session.save()

        response = Client().get(reverse('get-session-data-ajax') + "?q=user-id")

        self.assertEqual(json.loads(response.getvalue().decode())['user-id'], None)

    def test_set_session_data_ajax_url_exists(self):
        response = Client().post(reverse('set-session-data-ajax'))
        self.assertEqual(response.status_code, 200)

    def test_set_session_data_ajax_url_use_set_session_data_ajax_view(self):
        set_session_data_ajax_url = resolve(reverse('set-session-data-ajax'))
        self.assertEqual(set_session_data_ajax_url.func, set_session_data_ajax)